#include <Wire.h>
#include "DS3231.h"

RTClib RTC;

// SCREEN
#include "logoswarm.cpp"
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


#define LOGO_HEIGHT   64
#define LOGO_WIDTH    128




void setup() {
  Serial.begin(9600);
  Serial.println("Hola");
  Wire.begin();

  //OLED screen
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.drawBitmap(    0,    0,    logomini, LOGO_WIDTH, LOGO_HEIGHT, 1);
  display.display();
}


void loop() {
  
    DateTime now = RTC.now();
    
    display.clearDisplay();
    display.drawBitmap(    0,    0,    logomini, LOGO_WIDTH, LOGO_HEIGHT, 1);
    display.setTextSize(1);      // Normal 1:1 pixel scale
    display.setTextColor(SSD1306_WHITE); // Draw white text
    display.setCursor(0, 0);     // Start at top-left corner
    //display.cp437(true);         // Use full 256 char 'Code Page 437' font
    display.print(now.day(), DEC);
    display.print('/');
    display.print(now.month(), DEC);
    display.setCursor(100, 0);
    display.println(now.year(), DEC);
    display.setCursor(0, 55); 
    if (now.hour()<10){
      display.print(0);
      display.print(now.hour());
    } else{
      display.print(now.hour());
    }
    display.print(':');
    if (now.minute()<10){
      display.print(0);
      display.print(now.minute());
    } else{
      display.print(now.minute());
    }
    display.print(':');
    if (now.second()<10){
      display.print(0);
      display.print(now.second());
    } else{
      display.print(now.second());
    }
    display.println();
    display.display();

    
    
 

    
}
