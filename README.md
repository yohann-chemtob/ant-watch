# Ant watch

A rtc watch on an OLED screen to get an accurate time stamp during the experiments.

An arduino, an OLED screen and RTC chip (ChronoDot V2.1) all connected through I2S.

Picture here: https://twitter.com/ChemtobYohann/status/1331702071033286657

The library used here: https://github.com/NorthernWidget/DS3231

To initialize the correct time, use the DS3231_set.ino exemple in the library and my python code (don't forget to change the serial output according to your system).