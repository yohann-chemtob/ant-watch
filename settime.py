import time
import serial

s = serial.Serial('COM13',9600,timeout=5)
time.sleep(2)
s.flush()
localtime = time.localtime(time.time())

timetosent = str(localtime[0])[2:4]
for i in range(1,3):
    if len(str(localtime[i]))==1:
        tosend = '0' + str(localtime[i])
    else:
        tosend = str(localtime[i])
    timetosent += tosend

timetosent += "w"


for i in range(3,6):
    if len(str(localtime[i]))==1:
        tosend = '0' + str(localtime[i])
    else:
        tosend = str(localtime[i])
    timetosent += tosend

timetosent += "x"

print(timetosent)

s.write(str.encode(timetosent))

while True:
    print(s.readline())